import asyncio
from datetime import datetime

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()
app.lock = asyncio.Lock()
app.is_locked = False

async def work() -> None:
    await asyncio.sleep(3)


class TestResponse(BaseModel):
    elapsed: float


async def wait_for_unlock() -> None:
    """ I'll be honest, i'm not sure if there's more elegant and efficient method for serial calls for FASTAPI """
    """ Without this func all threads are getting deadlocked"""
    while app.lock.locked():
        await asyncio.sleep(0.01)


@app.get("/test", response_model=TestResponse)
async def handler() -> TestResponse:
    ts1 = datetime.now().timestamp()
    while app.is_locked:
        await wait_for_unlock()
    async with app.lock:
        app.is_locked = True
        await work()
        ts2 = datetime.now().timestamp()
        app.is_locked = False
    return TestResponse(elapsed=ts2 - ts1)
