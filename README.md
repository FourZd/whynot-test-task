How to run server:

1. Open terminal
1. Enter folder with docker-compose.yml
2. Write "docker compose up" into the terminal
3. Wait for server to start

Server address with test api: localhost:8000/test

How to run unittests:
1. Enter terminal
2. "docker exec -it app bash"  ! You should be sure that container is running !
3. "pytest tests.py"

Description of tests:
1. Test status (Success if 200)
2. Test response type (Success if float)
3. Test single api call (Success if elapsed time > 3 sec)
4. Test multiple api calls (Success if finish_time - start_time > 3 sec)

