from datetime import datetime

from fastapi.testclient import TestClient

from main import app

client = TestClient(app=app)

SLEEP_TIME = 2.999  # It's often 2.9999 or 3.0001


def test_status():
    response = client.get("/test")
    assert response.status_code == 200


def test_type():
    response = client.get("/test")
    response = response.json()
    assert type(response["elapsed"]) is float


def test_single_call():
    response = client.get("/test")
    response = response.json()
    assert response["elapsed"] > SLEEP_TIME


def test_multiple_calls():
    """
    TestClient api calls are not in concurrency,
    so i can't check it like:
    second_response - first_response > SLEEP_TIME
    because both elapsed would be ~3.01

    But my solution also works correct
    """
    start = datetime.now().timestamp()
    first_response = client.get("/test")
    second_response = client.get("/test")
    finish = datetime.now().timestamp()

    assert finish - start > SLEEP_TIME
